const fetch = require('node-fetch');

class Spotify{
    async init(){
        const res = await fetch('https://open.spotify.com/get_access_token?reason=transport');
        const json = await res.json();
        this.token = json.accessToken;
    }
    async isrc(id){
        const res = await fetch('https://api.spotify.com/v1/tracks/' + id, {
            headers: {
                'Authorization': 'Bearer ' + this.token
            }
        });
        const json = await res.json();
        return json?.external_ids?.isrc;
    }
    async upc(id){
        const res = await fetch('https://api.spotify.com/v1/albums/' + id, {
            headers: {
                'Authorization': 'Bearer ' + this.token
            }
        });
        const json = await res.json();
        return json?.external_ids?.upc;
    }
    async track(isrc){
        const res = await fetch('https://api.spotify.com/v1/search?type=track&q=isrc:' + isrc, {
            headers: {
                'Authorization': 'Bearer ' + this.token
            }
        });
        const json = await res.json();
        if(!json.tracks) return;
        if(json.tracks.items.length == 0) return;
        return json.tracks.items[0]?.external_urls?.spotify;
    }
    async album(upc){
        const res = await fetch('https://api.spotify.com/v1/search?type=album&q=upc:' + upc, {
            headers: {
                'Authorization': 'Bearer ' + this.token
            }
        });
        const json = await res.json();
        if(json.albums.items.length == 0) return;
        return json.albums.items[0]?.external_urls?.spotify;
    }
}

module.exports = Spotify;
