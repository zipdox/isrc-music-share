const fetch = require('node-fetch');

const options = {
    headers: {
        'Accept-Language': 'en-US,en'
    }
}

class Deezer{
    async isrc(id){
        const res = await fetch('https://api.deezer.com/2.0/track/' + id, options);
        const json = await res.json();
        return json.isrc;
    }
    async upc(id){
        const res = await fetch('https://api.deezer.com/2.0/album/' + id, options);
        const json = await res.json();
        return json.upc;
    }
    async track(isrc){
        const res = await fetch('https://api.deezer.com/2.0/track/isrc:' + isrc, options);
        const json = await res.json();
        return json.link;
    }
    async trackData(isrc){
        const res = await fetch('https://api.deezer.com/2.0/track/isrc:' + isrc, options);
        const json = await res.json();
        return json;
    }
    async album(upc){
        const res = await fetch('https://api.deezer.com/2.0/album/upc:' + upc, options);
        const json = await res.json();
        return json.link;
    }
    async albumData(upc){
        const res = await fetch('https://api.deezer.com/2.0/album/upc:' + upc, options);
        const json = await res.json();
        return json;
    }
}

module.exports = Deezer;
