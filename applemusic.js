const fetch = require('node-fetch');
const {JSDOM} = require('jsdom');

class AppleMusic{
    async init(){
        const res = await fetch('https://music.apple.com/us/browse');
        const text = await res.text();
        const dom = new JSDOM(text);
        const jsonText = [...dom.window.document.getElementsByTagName('meta')].find(elem=>elem.name=='desktop-music-app/config/environment').content;
        const json = JSON.parse(decodeURIComponent(jsonText));
        this.token = json.MEDIA_API.token;
    }
    async isrc(id){
        const res = await fetch('https://amp-api.music.apple.com/v1/catalog/us/?ids[songs]=' + id + '&l=en-us&platform=web', {
            headers: {
                "authorization": "Bearer " + this.token,
                "Origin": "https://music.apple.com"
            }
        });
        const json = await res.json();
        return json?.data[0]?.attributes?.isrc;
    }
    async upc(id){
        const res = await fetch('https://amp-api.music.apple.com/v1/catalog/us/?ids[albums]=' + id + '&l=en-us&platform=web', {
            headers: {
                "authorization": "Bearer " + this.token,
                "Origin": "https://music.apple.com"
            }
        });
        const json = await res.json();
        return json?.data[0]?.attributes?.upc;
    }
    async track(isrc){
        const res = await fetch('https://amp-api.music.apple.com/v1/catalog/us/songs?filter[isrc]=' + isrc + '&l=en-us&platform=web', {
            headers: {
                "authorization": "Bearer " + this.token,
                "Origin": "https://music.apple.com"
            }
        });
        const json = await res.json();
        return json?.data[0]?.attributes.url;
    }
    async album(upc){
        const res = await fetch('https://amp-api.music.apple.com/v1/catalog/us/albums?filter[upc]=' + upc + '&l=en-us&platform=web', {
            headers: {
                "authorization": "Bearer " + this.token,
                "Origin": "https://music.apple.com"
            }
        });
        const json = await res.json();
        return json?.data[0]?.attributes.url;
    }
}

module.exports = AppleMusic;
