# ISRC Music Share

Website for sharing music across different music streaming services. Simply append the song or album's URL or ISRC/UPC to the address and you'll get links to the song on several streaming services.

https://music.zipdox.net/

## Supported services

- Spotify
- Deezer
- Apple Music
