const express = require('express');
const {JSDOM} = require('jsdom');
const fs = require('fs');

const Deezer = require('./deezer.js');
const Spotify = require('./spotify.js');
const AppleMusic = require('./applemusic.js');

const isrcRegex = /^[A-Z]{2}\w{3}\d{2}\d{5}$/;
const upcRegex = /^[0-9]{12}$/;
const idRegex = /^[a-zA-Z0-9]+$/;

const app = express();

const deezer = new Deezer();
const spotify = new Spotify();
const appleMusic = new AppleMusic();

let notFound;

let template;
function formatPage(title, artist, isrc, cover, urls, type){
    const dom = new JSDOM(template);
    const document = dom.window.document;

    document.title = title;
    document.getElementById('title').content = title;
    document.getElementById('desc').content = artist;
    document.getElementById('title2').textContent = title;
    document.getElementById('artist').textContent = artist;
    document.getElementById('isrc').textContent = isrc;
    document.getElementById('cover').src = cover;
    document.getElementById('ogcover').content = cover;
    document.getElementById('ogtype').content = type;
    document.body.insertAdjacentHTML('beforeend', '<br>');

    for(let url of urls){
        let elem = document.createElement('a');
        elem.href = url;
        elem.textContent = url;
        document.body.appendChild(elem);
        document.body.insertAdjacentHTML('beforeend', '<br>');
    }
    return dom.serialize();
}

async function getCodeFromURL(query){
    let url;
    try{
        url = new URL(query);
    }catch(err){
        return null;
    }
    
    let isrc, upc, id;
    switch(url.host){
        case 'open.spotify.com':
            id = url.pathname.split('/track/')[1];
            if(id){
                if(!idRegex.test(id)) return 2;
                isrc = await spotify.isrc(id);
                return {isrc, type: 't'};
            }
            id = url.pathname.split('/album/')[1];
            if(id){
                if(!idRegex.test(id)) return 2;
                upc = await spotify.upc(id);
                return {upc, type: 'a'};
            }
            return 1;
        case 'deezer.com':
        case 'www.deezer.com':
            id = url.pathname.split('/track/')[1];
            if(id){
                if(!idRegex.test(id)) return 2;
                isrc = await deezer.isrc(id);
                return {isrc, type: 't'};
            }
            id = url.pathname.split('/album/')[1];
            if(id){
                if(!idRegex.test(id)) return 2;
                upc = await deezer.upc(id);
                return {upc, type: 'a'};
            }
            return 1;
        case 'music.apple.com':
            id = url.searchParams.get('i');
            if(id){
                if(!idRegex.test(id)) return 2;
                isrc = await appleMusic.isrc(id);
                return {isrc, type: 't'};
            }
            id = url.pathname.split(/\/album\/[^\/]+\//)[1];
            if(id){
                if(!idRegex.test(id)) return 2;
                upc = await appleMusic.upc(id);
                return {upc, type: 'a'};
            }
            return 1;
        default:
            return 0;
    }
    return isrc;
}

async function handleRequest(req, res){
    if(req.path == '/favicon.ico') return res.status(404).send();

    const query = req.originalUrl.slice(1);

    let deezerData, deezerLink, spotifyLink, appleMusicLink;

    if(isrcRegex.test(query)){
        deezerData = await deezer.trackData(query);
        deezerLink = deezerData.link;
        if(!deezerLink) return res.send(notFound);
        spotifyLink = await spotify.track(query);
        appleMusicLink = await appleMusic.track(query);
        return res.send(formatPage(deezerData.title, deezerData?.artist?.name, deezerData?.isrc, deezerData?.album?.cover_medium, [deezerLink, spotifyLink, appleMusicLink], 'music:song'));
    }

    if(upcRegex.test(query)){
        deezerData = await deezer.albumData(query);
        deezerLink = deezerData.link;
        if(!deezerLink) return res.send(notFound);
        spotifyLink = await spotify.album(query);
        appleMusicLink = await appleMusic.album(query);
        return res.send(formatPage(deezerData.title, deezerData?.artist?.name, deezerData?.upc, deezerData?.cover_medium, [deezerLink, spotifyLink, appleMusicLink], 'music:album'));
    }

    const code = await getCodeFromURL(query);
    if(code === null) return res.send('Not an ISRC, UPC or URL');
    if(code === 0) return res.send('Unsupported service');
    if(code === 1) return res.send('Unsupported URL scheme');
    if(code === 2) return res.send('Invalid track/album ID');

    if(code.type =='t'){
        if(code.isrc == undefined) return res.send(notFound);
        deezerData = await deezer.trackData(code.isrc);
        deezerLink = deezerData.link;
        spotifyLink = await spotify.track(code.isrc);
        appleMusicLink = await appleMusic.track(code.isrc);
        return res.send(formatPage(deezerData.title, deezerData?.artist?.name, deezerData?.isrc, deezerData?.album?.cover_medium, [deezerLink, spotifyLink, appleMusicLink], 'music:song'));
    }else if(code.type == 'a'){
        if(code.upc == undefined) return res.send(notFound);
        deezerData = await deezer.albumData(code.upc);
        deezerLink = deezerData.link;
        spotifyLink = await spotify.album(code.upc);
        appleMusicLink = await appleMusic.album(code.upc);
        return res.send(formatPage(deezerData.title, deezerData?.artist?.name, deezerData?.upc, deezerData?.cover_medium, [deezerLink, spotifyLink, appleMusicLink], 'music:album'));
    }else return res.send('Unable to find ISRC');
}

app.get('/*', handleRequest);

(async function(){
    template = await fs.promises.readFile('template.html', {encoding: 'utf8'});
    notFound = await fs.promises.readFile('notfound.html', {encoding: 'utf8'});

    await spotify.init();
    await appleMusic.init();

    setInterval(async function(){
        console.log('Refreshing tokens');
        spotify.init();
        appleMusic.init();
    }, 43200000);

    console.log('Ready!');

    app.listen(8008);
})();
